﻿using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using System.IO;
using UnityEditor;
using TMPro;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Reflection;
using System;
using System.Linq;

public class TestScript : UdonSharpBehaviour
{
    public TextMeshProUGUI TEST_TMP;

    void Start()
    {
        TEST_TMP.text = "Bye World!";
        TEST_TMP.fontSize = 20;
    }
}
